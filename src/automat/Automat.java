
package automat;


import java.util.*;
import java.io.*;

public class Automat {
    private String stare_initiala;
    private ArrayList<String> sir_stari_finale = new ArrayList<>();
    private final ListaTranzitii lista = new ListaTranzitii();
    
    Automat(String nume_fisier)
    {
        try 
        {
            BufferedReader read = new BufferedReader(new FileReader(nume_fisier));
            
            stare_initiala = read.readLine();       //citeste prima linie din fisier
            String sf = read.readLine();            //citeste a 2 a linie            
            String sfvect[]=sf.split(" ");          //creaza un vector din sf (element cu element)
            
            for(int i=0;i<sfvect.length;i++)
            {
                sir_stari_finale.add(sfvect[i]);    //multimea de stari finale formata anterior
            }                       
            while (true) 
            {               
                String tmp = read.readLine();       //citeste urmatoarele liniii
                if(tmp!=null)                       //daca nu e goala   
                {
                    String[] items = tmp.split(" ");//creez un vector cu fiecare element din tranzitie
                    
                    System.out.println("ObjT: "+items[0]+" "+items[1].charAt(0)+" "+items[2]);
                    
                    Tranzitie tr = new Tranzitie(items[0], items[1].charAt(0), items[2]);
                    lista.addTranzitie(tr);
                    //System.out.println(lista.size());
                    }
                else break;
            }
            System.out.println("Sunt " +lista.size());
        } 
        catch (Exception e) { 
            System.err.println("Nu s-a gasit fisierul " + e.getMessage());
        }
    }
    public boolean analizeazaCuvant(String cuvant_intrare) throws FileNotFoundException, IOException {
       
        String stare_actuala = stare_initiala; 
        for (int i = 0; i < cuvant_intrare.length(); i++) 
        {  
           System.out.println("st "+stare_actuala);
            char c = cuvant_intrare.charAt(i);
            String st = lista.findStareUrmatoare(stare_actuala, c);
            if (st != null) 
                {  
                stare_actuala = st;             
                }
            else 
                return false;
        }
        
        for (int i = 0; i < sir_stari_finale.size(); i++) { 
            if (stare_actuala.equals(sir_stari_finale.get(i))) {                 
                return true;
            }
            
        }
        return false;
    }
}