package automat;

import java.util.*;

public class ListaTranzitii {

    private ArrayList<Tranzitie> lista = new ArrayList<Tranzitie>();
    
    public void addTranzitie(Tranzitie tr) {
        lista.add(tr);
    }

    public int size()
    {
        return lista.size();
    }    
        
    public String findStareUrmatoare(String stare, char simbol) 
    {
        for (int i = 0; i < lista.size(); i++) 
        {
            Tranzitie tr = lista.get(i); 
            
            if (tr.getStareInceput().equals(stare) && tr.getSimbol() == simbol)
                return tr.getStareSfarsit();
        }
        return null;
    }
}