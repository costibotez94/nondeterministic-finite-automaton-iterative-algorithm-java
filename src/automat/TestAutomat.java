
package automat;

import java.io.IOException;
import javax.swing.*;

public class TestAutomat 
{
    public static void main(String[] args) throws IOException 
    {
        Automat automat = new Automat("C:/Users/Costin/Desktop/LFA-iterrativ/automat.txt"); 
    
        String cuvant = JOptionPane.showInputDialog(null,"Introduceti cuvantul de analizat:");
        
        if(cuvant!=null)
        {
            JOptionPane.showMessageDialog(null,"Cuvantul de analizat: "+cuvant);            
            if (automat.analizeazaCuvant(cuvant)==true) 
            {       
                JOptionPane.showMessageDialog(null,"Cuvantul <"+cuvant+"> a fost acceptat de automat.");
            } 
            else 
            {
                JOptionPane.showMessageDialog(null,"Cuvantul <"+cuvant+"> nu a fost acceptat de automat.");        
            }
        
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Nu ai introdus nici un cuvant de analizat!");
        }
    
    }
}