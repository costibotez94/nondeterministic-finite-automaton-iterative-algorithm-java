package automat;

public class Tranzitie 
{

    private final String stare_inceput;
    private final char simbol;
    private final String stare_sfarsit;

    public Tranzitie(String st_inceput, char sym, String st_sfarsit) 
    { 
        stare_inceput = st_inceput;
        simbol = sym;
        stare_sfarsit = st_sfarsit;
    }

    public String getStareInceput() 
    { 
        return stare_inceput;
    }

    public char getSimbol() 
    {
        return simbol;
    }

    public String getStareSfarsit() 
    {
        return stare_sfarsit;
    }
}